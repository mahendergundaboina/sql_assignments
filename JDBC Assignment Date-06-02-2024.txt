1. Insert a record by accepting the employee details from runtime?
------------------------------------------------------------------------------------
package day01;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import com.db.DbConnection;

public class InsertRecordByRuntime {

	public static void main(String[] args) {
		Connection connection = DbConnection.getConnection();
		Statement statement = null;
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the EmployeeId:");
		int empId = scanner.nextInt();
		System.out.println("Enter the EmployeeName:");
		String empName = scanner.next();
		System.out.println("Enter the Salary:");
		double salary = scanner.nextDouble();
		System.out.println("Enter the Gender:");
		String gender = scanner.next();
		System.out.println("Enter the emaild:");
		String emailId = scanner.next();
		System.out.println("Enter the Password:");
		String password = scanner.next();

		String insertQuery = "insert into employee values (" + 
							empId + " , '" + empName + "' ," + salary + ", '" + 
							gender + "', '" + emailId + "' , '" + password + "')";
		
		try {
			
			statement = connection.createStatement();
			int result = statement.executeUpdate(insertQuery);
			
			if (result > 0){
				System.out.println(result + "Record(s) is inserted");
			} else {
				System.out.println("Insertion failed");
			}
			
		} catch (SQLException e) {
			
			e.printStackTrace();
			
		}
		
		try {
			if(connection != null){
				statement.close();
				connection.close();
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

}
============================================================

2. Fetch the employee record based on empId?
-----------------------------------------------------------
package day01;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import com.db.DbConnection;

public class FetchEmployeeById {

	public static void main(String[] args) {
		Connection connection = DbConnection.getConnection();
		Statement statement = null;
		ResultSet resultSet = null;

		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter EmployeeId: ");
		int empId = scanner.nextInt();
		//		System.out.println();108

		String fetchQuery = "select * from employee where empId = "+  empId;

		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(fetchQuery);

			if (resultSet.next()) {

				System.out.println("EmpId   : " + resultSet.getInt(1));
				System.out.println("EmpName : " + resultSet.getString(2));
				System.out.println("Salary  : " + resultSet.getDouble(3));
				System.out.println("Gender  : " + resultSet.getString(4));
				System.out.println("EmailId : " + resultSet.getString(5));
				System.out.println("Password: " + resultSet.getString(6));
				System.out.println();


			} else {
				System.out.println("Record not Found...");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		try {
			if (connection != null) {
				resultSet.close();
				statement.close();
				connection.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
}
============================================================

3. Show All Employee Id's, Fetch the employee record based on empId?
--------------------------------------------------------------------------------------------
package day01;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import com.db.DbConnection;

public class FetchOneEmployeeFromAllIds {

	public static void main(String[] args) {

		Connection connection = DbConnection.getConnection();
		Statement statement = null;
		ResultSet resultSet = null;

		String selectQuery = "select empId from employee order by empId asc";

		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(selectQuery);

			if (resultSet != null) {

				while (resultSet.next()) {

					System.out.print(resultSet.getInt(1) + " ");

				}

			} else {
				System.out.println("No Record(s) Found!!!");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		System.out.println();

		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter EmployeeId: ");
		int empId = scanner.nextInt();

		String fetchQuery = "select * from employee where empId = " + empId;

		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(fetchQuery);

			if (resultSet.next()) {

				System.out.println("EmpId   : " + resultSet.getInt(1));
				System.out.println("EmpName : " + resultSet.getString(2));
				System.out.println("Salary  : " + resultSet.getDouble(3));
				System.out.println("Gender  : " + resultSet.getString(4));
				System.out.println("EmailId : " + resultSet.getString(5));
				System.out.println("Password: " + resultSet.getString(6));
				System.out.println();

			} else {
				System.out.println("Record not Found...");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		try {
			if (connection != null) {
				resultSet.close();
				statement.close();
				connection.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
 
	}

}


